# Information

The code in this directory can be executed as ROOT macro within ROOT but it
can also be compiled into a normal executable.
It is needed to setup the runtime environment first.

## Run as ROOT macro

```
root -l CalcVolumeArray.C
root -l CalcVolume.C
```

## Compile commands in GSI environment

```
g++ --std=c++17 -I$ROOTSYS/include CalcVolumeArray.C -o CalcVolumeArray
g++ --std=c++17 -I$ROOTSYS/include CalcVolume.C -o CalcVolume
```
