# Information

The code in this directory can be executed as ROOT macro within ROOT but it
can also be compiled into a normal executable.
It is needed to setup the runtime environment first.

When using unique pointers it is necessary to have a break point at the end
of the fucntion. Otherwise the created objects are automatically destroyed 
when leaving the scope of the function. Normally this isn't what is expected
since one would like to examine the created histograms.  

## Run as ROOT macro

root -l ThrowTheDices.C
root -l ThrowTheDicesFlexible.C

## Compile commands in GSI environment

g++ --std=c++17 -I$ROOTSYS/include/root -L$ROOTSYS/lib -lCore -lHist -lGpad -lMathCore ThrowTheDices.C -o ThrowTheDices
g++ --std=c++17 -I$ROOTSYS/include/root -L$ROOTSYS/lib -lCore -lHist -lGpad -lMathCore ThrowTheDicesFlexible.C -o ThrowTheDicesFlexible

