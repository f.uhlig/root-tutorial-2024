#include <iostream>
#include <memory>

int uniquePointersAndMemory(int val1, int val2)
{
  std::unique_ptr<int> e{new int(val1)};
  std::unique_ptr<int> f{new int(val2)};

  std::cout << "The unique pointer e points to the address " << e.get() 
            << " which has the value " << *e << std::endl; 

  std::cout << "The unique pointer f points to the address " << f.get() 
            << " which has the value " << *f << std::endl; 

  std::cout << std::endl;

  return 0;
}

int main() 
{
  uniquePointersAndMemory(0,6576);
  uniquePointersAndMemory(1,333);
}

