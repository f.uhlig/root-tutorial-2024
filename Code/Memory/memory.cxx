#include <iostream>
#include <memory>
#include <vector>

int uniquePointersAndMemory()
{
  std::unique_ptr<int> e{new int(0)};
  std::unique_ptr<int> f{new int(6575)};

  std::cout << "The unique pointer e points to the address " << e.get() 
            << " which has the value " << *e << std::endl; 

  std::cout << "The unique pointer f points to the address " << f.get() 
            << " which has the value " << *f << std::endl; 

  return 0;
}

int pointersAndMemory(bool delPointers=false)
{
  int* c = new int(0);
  int* d = new int(6575);

  std::cout << "The raw pointer c points to the address " << c
            << " which has the value " << *c << std::endl; 

  std::cout << "The raw pointer d points to the address " << d
            << " which has the value " << *d << std::endl; 

  if (delPointers) {
    delete c;
    delete d;
  }

  return 0;
}

int variablesAndMemory(int val_a = 0, int val_b = 6575)
{
  int a=val_a;
  int b{val_b};

  std::cout << "The variable a has the value " << a 
            << " and is stored at the address " << &a << std::endl; 

  std::cout << "The variable b has the value " << b 
            << " and is stored at the address " << &b << std::endl; 

  return 0;
}

int STLContainersAndMemory(int val)
{
  std::vector<int> b{val};

  std::cout << "The address of the vector b is " << &b 
            << " with the data stored at " << b.data() << std::endl;
  std::cout << "The value of the first element of vector b is " << b.at(0)
            << std::endl;

  return 0;
}

int memory()
{
  std::cout << "First time calling variablesAndMemory." <<std::endl;
  variablesAndMemory();
  std::cout <<std::endl;

  std::cout << "Second time calling variablesAndMemory." <<std::endl;
  variablesAndMemory(1, 343);
  std::cout <<std::endl;

  std::cout << "Calling pointersAndMemory without deleting the pointers" <<std::endl;
  pointersAndMemory();
  std::cout <<std::endl;

  std::cout << "Calling pointersAndMemory deleting the pointers" <<std::endl;
  pointersAndMemory(true);
  std::cout <<std::endl;

  std::cout << "First time calling uniquePointersAndMemory" <<std::endl;
  uniquePointersAndMemory();
  std::cout <<std::endl;

  std::cout << "Second time calling uniquePointersAndMemory" <<std::endl;
  uniquePointersAndMemory();
  std::cout <<std::endl;

  std::cout << "First time calling STLContainersAndMemory" <<std::endl;
  STLContainersAndMemory(6575);
  std::cout <<std::endl;

  std::cout << "Second time calling STLContainersAndMemory" <<std::endl;
  STLContainersAndMemory(3333);
  std::cout <<std::endl;


  return 0;
}

int main() 
{
  return memory();
}

