#include <iostream>

int allocateOnStack(int array_size)
{
    std::cout << "Allocating an in array with size " << array_size <<
std::endl;
    int myStack[array_size];
    myStack[0]=1;
    std::cout << "hi: " << myStack[0] << std::endl; // we'll use myStack[0] here so the compiler won't optimize the array away

    return 0;
}

int main()
{
  allocateOnStack(1);
  allocateOnStack(10000);
  allocateOnStack(10000000);
  return 0;
}